FROM katharostech/nodejs

RUN mkdir /nodejs

# The package.json file that details the npm packages needed by the zen theme
COPY package.json /nodejs/package.json

WORKDIR /nodejs

# Install all of the alpine and npm packages that are needed then delete
# the unnecessary ones when we are done.
RUN \
apk update --no-cache && \
apk add --no-cache g++ make python && \
npm install && \
npm install -g gulp-cli && \
apk del g++ make python

COPY gulpfile.js /nodejs/gulpfile.js
RUN chmod 744 /nodejs/gulpfile.js

COPY docker-entrypoint.sh /docker-entrypoint.sh

# By default we will run an interactive shell when you run the container
CMD ["sh"]

# The entrypoint script makes sure the node_modules are installed then
# runs the command that is passed in to the container.
ENTRYPOINT ["sh", "-c"]
