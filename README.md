# What is this for? #

This container was specifically designed to build the Drupal theme Zen, though you could use it for any sass project that needs the sass plugins that zen uses. It runs nodejs and gulp and will automatically build the sass source to css. You can also run all of the other gulp tasks that come with zen.'


# How to run this container #

When you run the container you must mount the root of the zen theme into the container at /nodejs/theme. For example:


```
#!bash
cd pathToMyTheme
docker run --name gulp -it -v $(pwd):/nodejs/theme katharostech/zen_nodejs
```

This will put you into an interactive console where you can run gulp commands just like you would normally. Or if you want you can pass the gulp commands into the container when you run it. For example, you could run "gulp watch" to have the container watch your sass for changes and compile when you make updates:


```
#!bash
cd pathToMyTheme
docker run --name gulp -d -v $(pwd):/nodejs/theme katharostech/zen_nodejs "gulp watch:css"
```

I have found it useful just to leave the container running like this and just start it and stop it when I need to so that I don't have to re-run the container.

**Note:** You may notice that, in the above command, I put "gulp watch:css" in quotes. You must put the commands that you want to run in the container inside of quotes for some reason. I might try to fix that some time.

# Thanks! #

I hope you find this as useful as I did! Docker is awesome!