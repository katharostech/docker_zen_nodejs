# Copy the node_modules into the zen sub-theme if they are not already there
#
# The funky syntax is to tell the copy command, in interactive mode(-i), not
# to override the node_modules directory if it already exists
echo "n" | cp -ir /nodejs/node_modules /zen/node_modules 2> /dev/null

# Run the passed in cmd
sh -c "$@"
